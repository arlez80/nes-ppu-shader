/*
	NES PPU風シェーダー by あるる（きのもと 結衣）
	NES PPU-esque Shader by @arlez80

	MIT License
*/

shader_type canvas_item;
render_mode unshaded;

// 色度信号の最大値
const vec2 iq_unit = vec2( 0.5957, 0.5226 );

// 輝度段階
uniform float luma_step = 4.0;
// 色度段階
uniform float color_step = 12.0;
// 色度強さ
uniform float color_scale : hint_range( 0.0, 1.0 ) = 1.0;
// ディザリング
uniform float dithering : hint_range( 0.0, 1.0 ) = 0.025;
// ディザリングパターンを毎フレーム変えるか？
uniform bool dithering_frame_change = false;

float random( vec2 pos )
{ 
	return fract(sin(dot(pos, vec2(12.9898,78.233))) * 43758.5453);
}

void fragment( )
{
	vec3 color = texture( SCREEN_TEXTURE, SCREEN_UV ).rgb;

	// RGB -> YIQ
	float y = clamp( dot( color, vec3( 0.299, 0.587, 0.114 ) ) + dithering * random( SCREEN_UV + vec2( TIME * float( dithering_frame_change ), 0.0 ) ), 0.0, 1.0 );
	vec2 iq = vec2(
		dot( color, vec3( 0.596, -0.274, -0.322 ) )
	,	dot( color, vec3( 0.211, -0.523, 0.312 ) )
	) / iq_unit;

	// 減色
	y = floor( y * luma_step ) / luma_step;
	iq = floor( iq * color_step ) / color_step * iq_unit * color_scale;

	// YIQ -> RGB
	COLOR = vec4(
		dot( vec3( y, iq ), vec3( 1.0, 0.956, 0.621 ) )
	,	dot( vec3( y, iq ), vec3( 1.0, -0.272, -0.647 ) )
	,	dot( vec3( y, iq ), vec3( 1.0, -1.106, 1.703 ) )
	,	1.0
	);
}
